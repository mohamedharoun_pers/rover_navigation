<?php namespace App\Helpers;

class DirectionsHelper{

    /**
     * @param $currentLocation
     * @param $destination
     * @return array
     */
    public static function getLocation($currentLocation, $destination){
        $x_axis = $currentLocation[0];
        $y_axis = $currentLocation[1];
        $direction = $currentLocation[2];
        $dest_direction = '';
        $dest_location_x = '';
        $dest_location_y = '';

        switch ($destination){
            case 'L':
                {
                    switch ($direction){
                        case 'N':
                            $dest_direction ='W';
                            $dest_location_x = $x_axis;
                            $dest_location_y = $y_axis;
                        break;
                        case 'W':
                            $dest_direction ='S';
                            $dest_location_x = $x_axis;
                            $dest_location_y = $y_axis;
                        break;
                        case 'S':
                            $dest_direction ='E';
                            $dest_location_x = $x_axis;
                            $dest_location_y = $y_axis;
                        break;
                        case 'E':
                            $dest_direction ='N';
                            $dest_location_x = $x_axis;
                            $dest_location_y = $y_axis;
                        break;

                    }
                }
            break;
            case 'R':
                {
                    switch ($direction){
                        case 'N':
                            $dest_direction ='E';
                            $dest_location_x = $x_axis;
                            $dest_location_y = $y_axis;
                            break;
                        case 'E':
                            $dest_direction ='S';
                            $dest_location_x = $x_axis;
                            $dest_location_y = $y_axis;
                            break;
                        case 'S':
                            $dest_direction ='W';
                            $dest_location_x = $x_axis;
                            $dest_location_y = $y_axis;
                            break;
                        case 'W':
                            $dest_direction ='N';
                            $dest_location_x = $x_axis;
                            $dest_location_y = $y_axis;
                            break;
                    }
                }
                break;
            case 'M':
                {
                    switch ($direction){
                        case 'N':
                            $dest_location_y = $y_axis +1;
                            $dest_location_x = $x_axis;
                            $dest_direction = $direction;
                            break;
                        case 'W':
                            $dest_location_y = $y_axis;
                            $dest_location_x = $x_axis-1;
                            $dest_direction = $direction;
                            break;
                        case 'S':
                            $dest_location_y = $y_axis -1;
                            $dest_location_x = $x_axis;
                            $dest_direction = $direction;
                            break;
                        case 'E':
                            $dest_location_y = $y_axis;
                            $dest_location_x = $x_axis+1;
                            $dest_direction = $direction;
                            break;

                    }

                }
                break;

        }

        return [$dest_location_x,$dest_location_y,$dest_direction];
    }
}
