<?php

namespace App\Http\Controllers;

use App\Helpers\DirectionsHelper;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class RoverController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        list($x_coordinate,$y_coordinate, $current_direction) = explode(',',$request->coordinates);
        $directions = explode(',',$request->directions);
        $locationArray = [];

        foreach ($directions as $key => $direction){
            $locationArray = DirectionsHelper::getLocation([$x_coordinate,$y_coordinate,$current_direction], $direction);
            $x_coordinate = $locationArray[0];
            $y_coordinate = $locationArray[1];
            $current_direction = $locationArray[2];
        }

        return view('plateau', ['roverLocation' => $locationArray]);
    }

}
